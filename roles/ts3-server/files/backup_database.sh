#!/bin/bash

cd /opt/ts3-db
mysqldump -u teamspeak3 \
    -pfoobarbaz teamspeak3 \
    --skip-comments \
    --skip-create-options \
    --compact > ts3.sql

git add ts3.sql
git commit -m "Backup `date`"
git push origin master

