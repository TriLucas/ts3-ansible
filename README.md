# Ansible managed Teamspeak3 

This playbook will automagically set up your server to be a fully functional
Teamspeak 3 server instance (without a license)

## Usage

Run this command to install everything necessary on a remote machine:

```
$ ansible-playbook spawn-ts3.yml -i <server address>, -u <privileged user> -k
```

When using a GitLab repository as a database backup, create a deployment key
in the GitLab UI, copy the private key into the `ts3-ansible` root folder and
provide the private key's filename as an extra variable:

```
$ ansible-playbook spawn-ts3.yml -i <server address>, -u <privileged user> -k --extra-vars "gitlab_key=<keyname>"
```
